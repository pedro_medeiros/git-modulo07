public class ContaSalario extends Conta {
    private int quantidadeSaques;
    public ContaSalario(String cliente, int numero, int agencia, String banco, double saldo, int quantidadeSaques) {
		super(cliente, numero, agencia, banco, saldo);
		this.cliente = cliente;
		this.numero = numero;
		this.agencia = agencia;
		this.banco = banco;
		this.saldo = saldo;
		this.quantidadeSaques = quantidadeSaques;
	}
	@Override
	public double getSaldo() {
		return this.saldo;
	}
	@Override
	public String toString() {
		return "ContaSalario [cliente=" + cliente + ", numero=" + numero + ", agencia=" + agencia + ", banco=" + banco +
				", saldo=" + saldo + ",quantidadeSaques=" + quantidadeSaques + "]";
	
	}
	public boolean sacar(double quantia) {
		if(quantia>saldo) {
			return false;
		}
		else {
			if(this.quantidadeSaques >0) {
				this.quantidadeSaques--;
				this.saldo -= quantia;
				return true;
			}
			else {
				System.out.println("Limite de saques extendidos");
				return false;
			}
        }
    }