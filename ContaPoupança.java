public class ContaPoupança extends Conta{
	private int diaAniversario;
	private double taxaDeJuros;
	public ContaPoupança(String cliente, int numero, int agencia, String banco, double saldo, int diaAniversario, double taxaDeJuros) {
		super(cliente, numero, agencia, banco, saldo);
		this.cliente = cliente;
		this.numero = numero;
		this.agencia = agencia;
		this.banco = banco;
		this.saldo = saldo;
		this.diaAniversario = diaAniversario;
		taxaDeJuros = taxaDeJuros;
	}
    public double getSaldo(double saldo) {
		if (saldo >= this.diaAniversario) {
			saldo = this.saldo + this.saldo*this.taxaDeJuros;
			return this.saldo;
		}
		else 
			return this.saldo;
	}
	@Override
	public boolean sacar(double quantia) {
		this.saldo = quantia;
				return true;
	}
	@Override
	public String toString() {
		return "ContaPoupança [cliente=" + cliente +", numero=" + numero + ", agencia=" + agencia + ", banco=" + banco
				+ ", saldo=" + saldo +" diaAniversario=" + diaAniversario + ", TaxaDeJuros=" + taxaDeJuros + "]";
	
	}
	}