public class TestaConta{

	public static void main(String[] args) {
		Conta Contas[] = new Conta[3];
		ContaCorrente cc = new ContaCorrente("João", 1, 1, "Banco 1", 100, 1000);
		ContaPoupança cp = new ContaPoupança("Roberto", 2, 1, "Banco 1", 200, 20, 0.05);
		ContaSalario cs = new ContaSalario("Lucas", 3, 1, "Banco 1", 1000, 3);
		
		Contas[0] = cc;
		Contas[1] = cp;
		Contas[2] = cs;
    
    cc.sacar(50);
	cp.depositar(50);
	System.out.println(cc);
	System.out.println(cp);
    }
}