public class Conta {
	protected String cliente;
	protected int numero;
	protected int agencia;
	protected String banco;
	protected double saldo;
	
	public double getSaldo() {
		return this.saldo;
	}
	
	public Conta(String cliente, int numero, int agencia, String banco, double saldo) {
		super();
		this.cliente = cliente;
		this.numero = numero;
		this.agencia = agencia;
		this.banco = banco;
		this.saldo = saldo;
	}
	
	@Override
	public String toString() {
		return "Conta [cliente=" + cliente + ", numero=" + numero + ", agencia=" + agencia + ", banco=" + banco
				+ ", saldo=" + saldo + "]";
	}

	public boolean sacar(double quantia) {
		return false;
	}
	
	public void depositar(double quantia) {
		this.saldo += quantia;
	}}