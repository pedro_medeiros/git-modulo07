public class ContaCorrente extends Conta {					
	private double chequeEspecial;

	public ContaCorrente(String cliente, int numero, int agencia, String banco, double saldo,double chequeEspecial) {
		super(cliente, numero, agencia, banco, saldo);
		this.cliente = cliente;
		this.numero = numero;
		this.agencia = agencia;
		this.banco = banco;
		this.saldo = saldo;
		chequeEspecial = chequeEspecial;
	}
	
	@Override
	public String toString() {
		return "ContaCorrente [chequeEspecial=" + chequeEspecial + "]";

	}
	
	@Override
	public boolean sacar(double quantia) {
		double disponivel = this.chequeEspecial + this.saldo;
		if (quantia > disponivel) {
			System.out.println("Você não tem limite disponivel.");
			return false;
		}
		else {
			this.saldo = this.saldo - quantia;
			return true;
		}
		}
		public double getSaldo() { 
			return this.chequeEspecial + this.saldo;
			}
		}